#!/usr/bin/bash

MYHOSTNAME=$(hostnamectl hostname --static)
MYIP=$(ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p')
NFSVERS_LIST="4.2 4.1 4.0 3"

echo "Setup..."
cp /etc/exports /etc/exports.bak
cp /etc/nfs.conf /etc/nfs.conf.bak
mkdir -p /export_test /mnt_test
echo "nfs_test" > /export_test/testfile
systemctl start nfs-server

echo "Check export and mount"
echo "/export_test *(ro,insecure)" > /etc/exports
exportfs -a
showmount -e | grep -q export_test
[ $? -ne 0 ] && echo "Failed to export nfs" && exit 1
for VERS in $NFSVERS_LIST; do
	for SERV in $MYHOSTNAME $MYIP; do
		mount -t nfs -o vers=$VERS $SERV:/export_test /mnt_test
		[ $? -ne 0 ] && echo "Failed to mount" && exit 1
		cat /proc/mounts | grep -q mnt_test
		[ $? -ne 0 ] && echo "Failed to find" && exit 1
		umount /mnt_test
	done
done

echo "Check simple IO"
exportfs -i -o rw,secure,no_root_squash *:/export_test
showmount -e | grep -q export_test
[ $? -ne 0 ] && echo "Failed to export nfs" && exit 1
for VERS in $NFSVERS_LIST; do
	for SERV in $MYHOSTNAME $MYIP; do
		mount -o nfsvers=$VERS $SERV:/export_test /mnt_test
		[ $? -ne 0 ] && echo "Failed to mount" && exit 1
		cat /proc/mounts | grep -q mnt_test
		[ $? -ne 0 ] && echo "Failed to find" && exit 1
		cat /mnt_test/testfile | grep -q nfs_test
		[ $? -ne 0 ] && echo "Failed to read" && exit 1
		date >> /mnt_test/testfile 2>/dev/null
		[ $? -ne 0 ] && echo "Failed to write" && exit 1
		umount /mnt_test
	done
done

echo "Check nfs negotiation"
echo -e "[nfsd]\n vers3=y\n vers4=n" > /etc/nfs.conf
if [ -f /etc/nfsmount.conf.d/10-nfsv4.conf ]; then
	mv /etc/nfsmount.conf.d/10-nfsv4.conf /etc/nfsmount.conf.d/10-nfsv4.conf.disabled
fi
systemctl restart nfs-server
showmount -e | grep -q export_test
[ $? -ne 0 ] && echo "Failed to export nfs" && exit 1
for SERV in $MYHOSTNAME $MYIP; do
	mount.nfs4 $SERV:/export_test /mnt_test >/dev/null 2>&1
	[ $? -eq 0 ] && echo "All v4 disabled but mounted" && exit 1
	mount.nfs $SERV:/export_test /mnt_test
	[ $? -ne 0 ] && echo "Failed to fallback" && exit 1
	cat /proc/mounts | grep -q mnt_test
	[ $? -ne 0 ] && echo "Failed to find" && exit 1
	umount /mnt_test
done

echo "Success"

echo "Cleanup..."
exportfs -ua
showmount -e | grep -q export_test
[ $? -eq 0 ] && echo "Failed to unexport nfs" && exit 1
systemctl stop nfs-server
cp /etc/nfs.conf.bak /etc/nfs.conf
cp /etc/exports.bak /etc/exports
rm -rf /export_test /mnt_test
if [ -f /etc/nfsmount.conf.d/10-nfsv4.conf.disabled ]; then
	mv /etc/nfsmount.conf.d/10-nfsv4.conf.disabled /etc/nfsmount.conf.d/10-nfsv4.conf
fi

exit 0
